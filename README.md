# Python: Countdown App


#### Project Outline

In this project we will write a python script which will accept a user input and then print back how much time is remaining until that deadline

#### Lets get started

In order to work with dates we need to implement the module for date

![Image1](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image1.png)

Can then set an input value

![Image2](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image2.png)

Can then save it into a variable as a user input

![Image3](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image3.png)

Can the convert the string to a list of strings,can then extract the goal and deadline into a variable and then print it out like the below

![Image4](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image4.png)

Can then input some dates and test the current code

![Image5](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image5.png)

However we want to input the date instead of the code interpreting it is a string

Can do that by referencing the module and then the definition which in this case is date.time

![Image6](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image6.png)

Can then implement the below which will then take the string and convert it into a date format

![Image7](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image7.png)

Here what happens is that we take the input of deadline which is the second array and then configure the format for it to be converted to a date format

![Image8](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image8.png)

![Image9](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image9.png)

Lets test and see if it display the date format in the below

![Image10](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image10.png)

Can then assign it to a variable

![Image11](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image11.png)

Now lets implement the deadline functionality, lets also include todays date and print the difference between todays date and deadline date

![Image12](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image12.png)

And can see it functioning below

![Image13](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image13.png)


Can then input it in to a variable and display a message

![Image14](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image14.png)

Can then execute it and can see the message

![Image15](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image15.png)

Can also specify it for days

![Image16](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image16.png)

Can also do it for hours and execute it

![Image17](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image17.png)

Can then implement the int function to get rid of the decimals

![Image18](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image18.png)

Can further optimize to re-work the module name and extract ‘hours_till’

![Image19](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image19.png)

And below are the results

![Image20](https://gitlab.com/FM1995/python-countdown-app/-/raw/main/Images/Image20.png)
